import { moduleForModel, test } from 'ember-qunit';

moduleForModel('router', 'Unit | Model | router', {
  // Specify the other units that are required for this test.
  needs: ['model:testbed']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
