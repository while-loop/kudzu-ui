import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  username: DS.attr('string'),
  password: DS.attr('string'),
  privateKey: DS.attr('string'),
  providerRef: DS.attr('string'),
  provider: DS.belongsTo('provider', { async: true })
});
