import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  cpus: DS.attr('number'),
  memory: DS.attr('number'),
  disk: DS.attr('number'),
  providerRef: DS.attr('string'),
  provider: DS.belongsTo('provider', { async: true }),

  summary: function() {
    //TODO: include cpus, memory, and disk in summary
    return this.get('name');
  }.property('name', 'cpus', 'memory', 'disk')
});
