import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  project: DS.belongsTo('project', { async: true }),
  provider: DS.belongsTo('provider', { async: true }),
  hosts: DS.hasMany('host', { async: true }),
  networks: DS.hasMany('network', { async: true }),
  links: DS.hasMany('link', { async: true }),
  //FIXME: uses the naming work around, see models/routerx.js
  router: DS.belongsTo('routerx', { async: true }),

  nonServiceNetworks: Ember.computed.filterBy('networks', 'service', false),

  serviceNetwork: function() {
    return this.get('networks').filterBy('service', true).get('firstObject');
  }.property('networks.@each.service')
});
