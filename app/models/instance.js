import DS from 'ember-data';

export default DS.Model.extend({
  createdOn: DS.attr('date'),
  destroyedOn: DS.attr('date'),
  status: DS.attr('string'),
  error: DS.attr('string'),
  providerRef: DS.attr('string'),
  providerData: DS.attr('string'),
  build: DS.belongsTo('build')
});
