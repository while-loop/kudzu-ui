import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  cidr: DS.attr('string'),
  service: DS.attr('boolean'),
  testbed: DS.belongsTo('testbed')
});
