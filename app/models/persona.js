import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  source: DS.attr('string'),
  sourceDriver: DS.attr('string'),
  defaultConfig: DS.attr('string'),
  provisioner: DS.belongsTo('provisioner', { async: true })
});
