import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  provisionDriver: DS.attr('string'),
  provisionConfig: DS.attr('string'),

  provisionDriverSimple: function() {
    var driver = this.get('provisionDriver');
    if (driver) {
      return driver.substr(driver.lastIndexOf('.') + 1);
    } else {
      return null;
    }
  }.property('provisionDriver'),
});
