import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  clock: Ember.inject.service('clock'),

  name: DS.attr('string'),
  state: DS.attr('string'),
  task: DS.attr('string'),
  submittedOn: DS.attr('date'),
  startsOn: DS.attr('date'),
  expiresOn: DS.attr('date'),
  destroyedOn: DS.attr('date'),
  error: DS.attr('string'),
  testbed: DS.belongsTo('testbed'),
  hostInstances: DS.hasMany('host-instance'),

  expired: function() {
    var expiresOn = this.get('expiresOn');
    return expiresOn && (expiresOn < new Date());
  }.property('expiresOn', 'clock.seconds'),

  //XXX: `isDestroyed` is reserved by Ember
  destroyed: function() {
    var destroyedOn = this.get('destroyedOn');
    return destroyedOn && (destroyedOn < new Date());
  }.property('destroyedOn', 'clock.seconds'),

  expirationWarning: function() {
    if (this.get('destroyed')) {
      return 'This build has been destroyed.';
    } else if (this.get('expired')) {
      return 'This build has expired. It will be destroyed soon.';
    }
    return null;
  }.property('destroyed', 'expired'),

  taskHuman: function() {
    var task = this.get('task') || 'none';
    return task.toLowerCase().replace('_', ' ');
  }.property('task'),

});
