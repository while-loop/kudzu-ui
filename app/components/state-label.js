import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'span',

  showIcon: true,
  showText: true,
  noStateIcon: 'minus text-muted',
  noStateText: 'none',

  iconClass: function() {
    var icon = 'question-sign text-muted';
    var state = this.get('state');
    if (!state) {
      icon = this.get('noStateIcon');
    } else if (state === 'NONE') {
      icon = 'minus text-muted';
    } else if (state === 'SCHEDULED') {
      icon = 'time text-warning';
    } else if (state === 'STARTED') {
      icon = 'cog text-info';
    } else if (state === 'ACTIVE') {
      icon = 'ok text-success';
    } else if (state === 'ERROR') {
      icon = 'alert text-danger';
    } else if (state === 'DESTROYED') {
      icon = 'trash';
    }
    return 'glyphicon glyphicon-' + icon;
  }.property('state'),

  text: function() {
    var state = this.get('state') || this.get('noStateText');
    return state.toLowerCase().replace('_', ' ');
  }.property('state')
});
