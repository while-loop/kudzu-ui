import DS from 'ember-data';

/**
* FIXME: this is a workaround for the fact that we can't name our Ember model
* "router" but the server can; see models/routerx.js
*/

export default DS.RESTSerializer.extend({
  modelNameFromPayloadKey: function(payloadKey) {
    return this._super('routerx');
  },
  payloadKeyFromModelName: function(modelName) {
    return this._super('router');
  }
});
