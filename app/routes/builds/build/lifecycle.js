import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('builds.build');
  },

  actions: {
    destroyBuild(build) {
      if (confirm(
`Are you sure you want to expire the build?

This action is irreversible.`
      )) {
        build.set('expiresOn', new Date());
        build.save();
      }
    }
  }
});
