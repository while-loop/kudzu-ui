import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    // grab the model of the parent route
    return this.modelFor('testbed');
  }
});
