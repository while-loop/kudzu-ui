import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('provider', params.provider_id);
  },

  actions: {
    save() {
      var model = this.controller.get('model');
      model.save();
      alert('Saved.');
      this.transitionTo('admin.providers');
    },

    cancel() {
      var model = this.controller.get('model');
      model.rollbackAttributes();
      this.transitionTo('admin.providers');
    }
  }
});
