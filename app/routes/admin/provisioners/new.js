import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    willTransition() {
      this.controller.set('newProvisioner', {});
    },

    save() {
      var self = this;
      var newProvisioner = this.controller.get('newProvisioner');
      var provisioner = this.store.createRecord('provisioner', newProvisioner);
      provisioner.save().then(function() {
        self.transitionTo('admin.provisioners');
      });
    },

    cancel() {
      this.transitionTo('admin.provisioners');
    }
  }
});
