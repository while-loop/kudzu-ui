import Ember from 'ember';

// based on http://livsey.org/blog/2013/02/20/tick-tock/

/**
The clock service is used to observe changes in time, for example to update computed
properties whose values change depending on the current time.
**/

export default Ember.Service.extend({
  second: null,
  minute: null,
  hour:   null,

  init: function() {
    this.tick();
  },

  tick: function() {
    var now = new Date();

    this.setProperties({
      second: now.getSeconds(),
      minute: now.getMinutes(),
      hour:   now.getHours()
    });

    Ember.run.later(this, this.tick, 1000);
  }
});
